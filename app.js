var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var path = require("path");

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var import_path = function(relative_path){
    return path.join(__dirname, relative_path);
}

var routes = require(path.join(__dirname, "/routes/index"));
var users = require('./routes/users');

var server_config = require("./config/servers");

var debug_settings = require("./config/debug_settings");

var DEBUG = debug_settings.DEBUG_ENABLED;

var moment = require('moment');

var http = require('http').Server(app);
var io = require('socket.io')(http);

var app = module.exports = express();

app["DEBUG"] = DEBUG;

var events = require('events');
var eventEmitter = new events.EventEmitter();

var redis   = require('redis');
rc = redis.createClient();

rc.on("connect", function() {
    rc.subscribe("TEST_RUN_STATUS");
});

//console.log(rc)

rc.on("message", function (channel, message) {
    var json_data = JSON.parse(message);
    //console.log(json_data);
//    console.log(channel)

    if(channel == "TEST_RUN_STATUS") {
        //console.log(json_data.receivers);
        for( var i = 0 ; i < json_data.receivers.length ; i++ ) {
            var receiver = json_data.receivers[i];
            var packet = json_data.content[receiver];
//            console.log(packet.remote_peers);
            //console.log(packet);
            console.log("Sending to: "+receiver);

            io.to(json_data.receivers[i]).emit("CHAT_MESSAGE_RECEIVED",packet);
        }
    }

    if(json_data.status == "SUCCESS"){
        if(channel == "TEST_RUN_STATUS"){
            var data = json_data.data;
            var sender_id = json_data.sender_id;
            var receivers = data.receivers;
            var sender = { id: data.sender_id, name: data.sender_name }
//            console.log(sender);
            receivers.push(sender);
            for(var i = 0 ; i < receivers.length; i++) {
//                console.log("Message Sending to: " + receivers[i]);
                var data_stream = {
                    recipient: receivers[i].id,
                    data: data,
                    sender_id: sender_id,
                    sender_name: receivers[i].name
                }
                console.log("message sending to: "+receivers[i].id);
                io.to(receivers[i]).emit("MESSAGE_RECEIVED",JSON.stringify(data_stream));
            }
        }
    }
});

var connected_clients = {};

//var subscribe_presence = require('child_process').fork('./subscribe_presence.js');

var on_socket_connection = function(socket)
{

    var user_session_data;

    if(DEBUG)
    {
        console.log("A client connected.");
    }

    socket.on('_disconnect',function(data)
    {
        console.log("Other tabs open!");
        user_model.update_online_status({ id: data.user_id, status: 1 });
    });

    socket.on('disconnect', function()
    {
        if(DEBUG)
        {
            console.log("Client disconnected.");
        }

    });

}

io.on("connection",on_socket_connection);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

io.set('authorization', function (handshakeData, accept) {

  if (handshakeData.headers.cookie) {

    //handshakeData.cookie = cookieParser.parse(handshakeData.headers.cookie);

    if(DEBUG){
        console.log(handshakeData.headers.cookie.sessionid);
    }

    }
  accept(null, true);
});
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', function(req,res,next)
    {
        
    });
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.listen(server_config.port, server_config.host, function(){
  console.log('listening on ' + server_config.host + ':' + server_config.port);
});

module.exports = app;
